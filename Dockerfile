FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install uwsgi
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

ENV PYTHONPATH "${PYTHONPATH}:/usr/src/app/oca"
ENV OPS_CONFIG_FILE "/usr/src/app/config.cfg"

EXPOSE 8000/tcp

CMD [ "uwsgi", "-s",  "0.0.0.0:8000", "--manage-script-name", "--mount", "/oca=oca:app" ]
