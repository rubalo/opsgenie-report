from datetime import datetime


def date_picker() -> datetime:
    """
    Select a oncall start date / time
    Returns:

    """
    default = datetime.now().strftime('%Y/%m/%d')
    start_date = input(f"Enter week start date  (e.g. 2019/05/01):\n"
                       f"({default}): ") or default

    return datetime.strptime(start_date, "%Y/%m/%d")
