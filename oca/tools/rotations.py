import calendar
import pytz
from datetime import datetime, time, timedelta


def get_rotation_period(restriction: dict,
                        starting: datetime = None,
                        tz_name: str = None):

    start_restriction = get_rotation_date(
        restriction['startDay'],
        restriction['startHour'],
        restriction['startMin'],
        starting=starting,
        tz_name=tz_name)
    end_restriction = get_rotation_date(
        restriction['endDay'],
        restriction['endHour'],
        restriction['endMin'],
        starting=start_restriction,
        tz_name=tz_name)

    return start_restriction, end_restriction


def get_rotation_date(day_name: str, start_hour: int, start_min: int,
                      starting: datetime = None,
                      tz_name: str = None) -> datetime:
    """
    Return the the date in datetime format
    The date is compute for days of the moving week starting current day
    :param starting:
    :param tz_name: Name of the timezone
    :type tz_name: str
    :param day_name: day_name
    :type day_name: str
    :param start_hour: Rotation start hour
    :type start_hour: int
    :param start_min: Rotation start min
    :type start_min: int
    :return: Start rotation time in ISO 8601 format
    :rtype: datetime
    """

    # Define timezone
    if tz_name:
        tz = pytz.timezone(tz_name)
    else:
        tz = pytz.timezone('UTC')

    # Get current date
    d = starting if starting else get_now()

    # get the next matching day
    while d.weekday() != get_day_of_week(day_name):
        d += timedelta(1)

    start_time = time(start_hour, start_min, 1)
    start_datetime = tz.localize(
        datetime.combine(d, start_time))

    return start_datetime


def get_day_of_week(day_name: str) -> int:
    """
    Get the day of the current week regarding the name
    :param day_name: Name of the day (e.g. 'monday')
    :type day_name:
    :return: Day number (0: monday, 1: tuesday...)
    :rtype: int
    """
    return getattr(calendar, day_name.upper())


def get_now() -> datetime:
    """
    Datetime.now accessor to ease mocking
    :return: Datetime
    """
    return datetime.now()


def print_local_time(date: datetime, tz: str):
    return date.astimezone(pytz.timezone(tz))


def get_oncall_periods(restrictions: list,
                       starting: datetime = None,
                       tz_name: str = NotADirectoryError):

    # Get rotation start time
    rotations_start_time = list()
    for restriction in restrictions:
        rotations_start_time.append(
                get_rotation_period(
                    restriction,
                    starting=starting,
                    tz_name=tz_name),
        )

    rotations_start_time.sort()
    return rotations_start_time
