$(document).ready(function() {

    // Get default start/end of last week
    var [s, e] = getLastShift();

    $('#from').datetimepicker({
        sideBySide: true,
        format: "YYYY-MM-DDTHH:mm:ssZ",
        defaultDate: s
    });
    $('#to').datetimepicker({
        sideBySide: true,
        format: "YYYY-MM-DDTHH:mm:ssZ",
        useCurrent: false, //Important! See issue #1075,
        defaultDate: e
    });
    $("#from").on("dp.change", function(e) {
        $('#to').data("DateTimePicker").minDate(e.date);
    });
    $("#to").on("dp.change", function(e) {
        $('#from').data("DateTimePicker").maxDate(e.date);
    });

    // Fetch button launch the alert query
    $("#fetch").on('click', function(event) {
        event.preventDefault(); // To prevent following the link (optional)
        var fdate = moment($("#from").val()).valueOf()
        var tdate = moment($("#to").val()).valueOf()
        var url = $("#url").val();
        getAlerts(url, fdate, tdate, "table-placeholder");
        document.getElementById("alerts").style.display = "block";
    });

});

function getLastShift() {
    var date = new Date();
    var day = date.getDay();
    var prevMonday;
    if(date.getDay() == 1){
        prevMonday = new Date().setDate(date.getDate() - 7);
    }
    else{
        prevMonday = new Date().setDate(date.getDate() - day + 1);
    }

    start_shift = new Date(prevMonday).setHours(18, 30, 0);

    date = new Date(start_shift);

    nextMonday = new Date().setDate(date.getDate() + 7);

    end_shift = new Date(nextMonday).setHours(9, 30, 0);

    return [start_shift, end_shift];
}

function getDate(element) {
    var date;
    try {
        date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
        date = null;
    }

    return date;
}

// Get the alerts and update the desired table
function getAlerts(base_url, fdate, tdate, dest) {
    var obj, dbParam, xmlhttp, x, y, txt = "",
        url;
    url = `${base_url}?fdate=${fdate}&tdate=${tdate}`;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url, true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("x=" + dbParam);
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            myObj = JSON.parse(this.responseText);
            txt += "<table id='alerts_table' class='display compact cell-border hover order-column row-border stripe '>";
            txt += "<thead>";
            txt += "<tr>";
            txt += "<th>Created at</th>";
            txt += "<th>Updated at</th>";
            txt += "<th>Status</th>";
            txt += "<th>Alert name</th>";
            txt += "<th>Duration</th>";
            txt += "<th>Owner</th>";
            txt += "<th>Id</th>";
            txt += "</tr>";
            txt += "</thead>";
            txt += "<tbody>";
            for (x in myObj) {
                var created_at = moment(myObj[x]['created_at']);
                var updated_at = moment(myObj[x]['updated_at']);
                var duration = moment.duration(updated_at.diff(created_at))
                var h_duration = duration.humanize();
                var date_format = "YYYY/D/M H:mm";
                txt += "<tr>";
                txt += "<td data-order=" + created_at + ">" + created_at.format(date_format);  + "</td>";
                txt += "<td data-order=" + updated_at + ">" + updated_at.format(date_format); + "</td>";
                txt += "<td>" + myObj[x]['status'] + "</td>";
                txt += "<td>#" +  myObj[x]['tiny_id'] + ":" + myObj[x]['message'] + "</td>";
                txt += "<td data-order=" + duration + ">" + h_duration + "</td>";
                txt += "<td>" + myObj[x]['owner'] + "</td>";
                txt += "<td>" + myObj[x]['id'] + "</td>";
                txt += "</tr>";
            }
            txt += "</tbody>";
            txt += "</table>";

            document.getElementById(dest).innerHTML = txt;
            $('#alerts_table').dataTable( {
              "paging": false,
               "order": [[ 0, "desc" ]],
               "columnDefs": [
                    { "visible": false,  "targets": [ 6 ] },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="https://criteo.app.opsgenie.com/alert/detail/'+ row[6]+'/details">' + data + '</a>';
                        },
                        "targets": 3
                    },

               ]
            });
        }
    };
};