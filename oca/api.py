import json

from flask import (
    Blueprint,
    current_app as app,
    request,
)

from oca.tools import converter
from oca.opsg.opsg_sdk_client import OpsgenieClient


bp = Blueprint('api', __name__, url_prefix='/api')


@bp.route('/')
def api():
    return "Api home"


@bp.route('/alerts', methods=('GET',))
def alert_list():

    fdate = int(request.args.get("fdate"))
    tdate = int(request.args.get("tdate"))

    api_key = app.config["OPSGENIE_API_KEY"]

    client = OpsgenieClient(api_key)

    app.logger.info(f"Looking for Alerts between {fdate} and {tdate}")
    result = [a.to_dict() for a in client.get_alerts(fdate, tdate)]

    return json.dumps(result, default=converter.alert_converter)
