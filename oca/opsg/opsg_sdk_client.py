import logging

from typing import Generator, List, TypeVar
from opsg.opsg_rest_api import OpsgenieRestApi
from opsgenie.swagger_client import AlertApi
from opsgenie.swagger_client.rest import ApiException
from opsgenie.swagger_client import configuration
from opsgenie.swagger_client.models import Alert

logger = logging.getLogger(__name__)

T = TypeVar('T', int, int)


class OpsgenieClient(OpsgenieRestApi):

    def __init__(self, api_key: str):
        super().__init__(api_key)

    def _get_alert_api(self) -> AlertApi:
        configuration.api_key['Authorization'] = self.api_key
        # You can use base64 version of apiKey instead of prefix
        configuration.api_key_prefix['Authorization'] = 'GenieKey'
        return AlertApi()

    def _compute_periods(self, fdate: int, tdate: int) -> List[T]:

        query = f'createdAt > {fdate} and createdAt < {tdate}'

        # Count alerts
        nb_alerts = int(self.alert_count(query)['count'])
        logger.info(f"Nb alerts: {nb_alerts} between {fdate} and {tdate}")

        if nb_alerts > 100:
            # Too much alerts to query,
            # the range should be divided
            logger.info("Splitting query period")
            delta = int((tdate - fdate) / 2)
            periods = self._compute_periods(fdate, fdate + delta) + self._compute_periods(tdate - delta, tdate)
            return periods
        else:
            return [(fdate, tdate), ]

    def get_alerts(self, fdate: int, tdate: int) -> Generator[Alert, None, None]:

        query = f'createdAt > {fdate} and createdAt < {tdate}'

        periods = self._compute_periods(fdate, tdate)

        client = self._get_alert_api()

        for start, end in periods:

            query = f'createdAt > {start} and createdAt < {end}'
            logger.info(f"Opsgenie query: {query}")
            # Default identifier_type is id
            response = client.list_alerts(
                limit=100,
                query=query,
                sort='createdAt')

            try:
                # Refer to ListAlertsResponse for more detailed data
                logger.debug('request id: {}'.format(response.request_id))
                logger.debug('took: {}'.format(response.took))
                for alert_response in response.data:
                    # print('alert_response.id: {}'.format(alert_response.id))
                    # print('alert_response.tiny_id: {}'.format(alert_response.tiny_id))
                    # print('alert_response.alias: {}'.format(alert_response.alias))
                    # print('alert_response.message: {}'.format(alert_response.message))
                    # print('alert_response.status: {}'.format(alert_response.status))
                    # print('alert_response.acknowledged: {}'.format(alert_response.acknowledged))
                    # print('alert_response.is_seen: {}'.format(alert_response.is_seen))
                    # print('alert_response.tags: {}'.format(alert_response.tags))
                    # print('alert_response.snoozed: {}'.format(alert_response.snoozed))
                    # print('alert_response.snoozed_until: {}'.format(alert_response.snoozed_until))
                    # print('alert_response.count: {}'.format(alert_response.count))
                    # print('alert_response.last_occurred_at: {}'.format(alert_response.last_occurred_at))
                    # print('alert_response.created_at: {}'.format(alert_response.created_at))
                    # print('alert_response.updated_at: {}'.format(alert_response.updated_at))
                    # print('alert_response.source: {}'.format(alert_response.source))
                    # print('alert_response.owner: {}'.format(alert_response.owner))
                    # print('alert_response.priority: {}'.format(alert_response.priority))
                    # print('alert_response.actions: {}'.format(alert_response.actions))
                    # print('alert_response.teams: {}'.format(alert_response.teams))
                    # print('alert_response.integration: {}'.format(alert_response.integration))
                    # print('alert_response.report: {}'.format(alert_response.report))
                    yield alert_response
            except ApiException as err:
                logger.error(
                    "Exception when calling AlertApi->list_alerts: %s\n" % err)

    def get_oncall_alerts(self, rotation_periods: list):
        # Get alerts for OnCall Rotation
        alerts = list()
        for start_dt, end_dt in rotation_periods:
            start_ts = int(start_dt.timestamp())
            end_ts = int(end_dt.timestamp())

            logger.info(f"Looking for Alerts between {start_dt} and {end_dt}")
            query = f'createdAt > {start_ts} and createdAt < {end_ts}'
            for alert in self.get_alerts(query=query):
                alerts.append(alert.to_dict())

        alerts.sort(key=lambda x: x["created_at"])

        return alerts
