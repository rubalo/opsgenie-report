import logging
import requests
import json

API_URL = "https://api.opsgenie.com"

logger = logging.getLogger(__name__)


class OpsgenieRestApi:
    """
    Opsgenie python sdk only have Alerts API. For other query we need to use
    the REST API
    https://github.com/opsgenie/opsgenie-python-sdk
    """

    def __init__(self, api_key: str):
        self.api_key = api_key

    def _call_api(self, path: str, payload: dict = None):
        """
        Call the rest API
        :param path: path
        :type path: str
        :param payload: payload
        :type payload: dict
        :return:
        :rtype:
        """
        url = f'{API_URL}/{path}'
        headers = {'Authorization': f'GenieKey {self.api_key}'}
        result = requests.get(url, headers=headers, params=payload)
        if result.status_code != 200:
            raise Exception(result.text)
        return json.loads(result.text)['data']

    def get_schedule_rotation(self, schedule_id: str, rotation_id: str):
        """
        https://docs.opsgenie.com/docs/schedule-rotation-api
        :param schedule_id: Identifier of the schedule
        :type schedule_id: str
        :param rotation_id: Id of the schedule rotation
        :type rotation_id: str
        :return:
        :rtype:
        """
        path = f'v2/schedules/{schedule_id}/rotations/{rotation_id}'
        payload = {'scheduleIdentifierType': 'id'}
        return self._call_api(path, payload)

    def list_rotations(self, schedule_id):
        """
        https://docs.opsgenie.com/docs/schedule-rotation-api
        :return:
        :rtype:
        """
        path = f'v2/schedules/{schedule_id}/rotations'
        payload = {'scheduleIdentifierType': 'id'}
        return self._call_api(path, payload)

    def get_schedule(self, identifier: str):
        """

        https://api.opsgenie.com/v2/schedules/:identifier
        :param identifier:
        :type identifier:
        :return:
        :rtype:
        """
        path = f'v2/schedules/{identifier}'
        payload = {'scheduleIdentifierType': 'name'}
        return self._call_api(path, payload)

    def list_schedules(self):
        """
        https://api.opsgenie.com/v2/schedules
        :return:
        :rtype:
        """
        path = f'v2/schedules'
        return self._call_api(path)

    def who_is_on_call(self, schedule_name: str, date: str = None):
        """
        https://docs.opsgenie.com/docs/who-is-on-call-api
        :param date: '2019-05-02T02:00:00+02:00'
        :type date: str
        :param schedule_name:
        :type schedule_name:
        :return:
        :rtype:
        """
        path = f'v2/schedules/{schedule_name}/on-calls'
        payload = {
            'scheduleIdentifierType': 'name',
        }
        if date:
            payload['date'] = date
        return self._call_api(path, payload)

    def alert_count(self, query: str):
        """
        https://docs.opsgenie.com/docs/alert-api#section-count-alerts
        :param query: 'createdAt > 1559686012000 and createdAt < 1560893982000'
        :type query: str
        """
        path = f'v2/alerts/count'
        payload = {
            'query': query,
        }
        return self._call_api(path, payload)
