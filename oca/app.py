import logging

from oca import app

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


if __name__ == '__main__':
    logger.info("Creating app")
    app.run(host=" 0.0.0.0")
