# Opsgenie report


Print all alerts that were created during an opsgenie schedule rotation

	export OPS_CONFIG_FILE=<Absolute path you your specific config file>
	
see config.py.example for a configuration example

Set a virtualenv

    virtualenv venv -p python3
    source venv/bin/activate
    
Install requirements

    pip install -r requirements.txt
    
    
Start application

    cd oca
    export PYTHONPATH="{$PYTHONPATH}:$(pwd)"
    FLASK_APP=app.py flask run --host=0.0.0.0
    
  
