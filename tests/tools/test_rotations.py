import oca.tools.rotations as rota
import datetime
import pytest

WEEKDAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
WEEKDAYS_ENUM = list(enumerate(WEEKDAYS))


@pytest.mark.parametrize("number, name", (x for x in WEEKDAYS_ENUM))
def test_get_day_of_current_week(number, name):
    assert rota.get_day_of_week(name) == number


@pytest.mark.parametrize("day, name", (x for x in WEEKDAYS_ENUM))
def test_get_rotation_start_date(day, name):
    # 2010-02-01 was a monday

    start_date = datetime.datetime(2010, 2, 1)

    rotation_date = rota.get_rotation_date(
        f'{name}',
        18,
        30,
        starting=start_date
    ).isoformat()

    expected_result = f"2010-02-0{int(day) + 1 }T18:30:01+00:00"

    assert rotation_date == expected_result
